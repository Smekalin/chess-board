//
//  AppDelegate.h
//  UIViewGeometryHomeWork
//
//  Created by Sergey on 15/08/16.
//  Copyright © 2016 Sergey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

