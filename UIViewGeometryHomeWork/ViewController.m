//
//  ViewController.m
//  UIViewGeometryHomeWork
//
//  Created by Sergey on 15/08/16.
//  Copyright © 2016 Sergey. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property(strong, nonatomic) UIView* centerView;
@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self addcenterSquare];
    [self addSquare];
    [self addPawns];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) addSquare {
    
    static int tagIndex = 101;
    
    CGFloat width = self.centerView.frame.size.width;
    CGFloat step = width / (CGFloat)4;
    CGFloat side = width / (CGFloat)8;
    
    for (int i = 0; i < 8; i++) {
        CGFloat dx = i % 2 ? 0 : (width / 8);
        CGFloat dy = i * side;
        for (CGFloat x = dx; x <= width - side; x+=step) {
            CGRect rect = CGRectMake(x, dy, side, side);
            UIView* view = [[UIView alloc] initWithFrame:rect];
            view.tag = tagIndex;
            view.backgroundColor = [UIColor blackColor];
            view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
            [self.centerView addSubview:view];
            ++tagIndex;
        }
    }
}

- (void) addcenterSquare {
    
    self.centerView = [[UIView alloc] init];
    self.centerView.backgroundColor = [UIColor brownColor];
    
    CGFloat centerWidth = MIN(self.view.frame.size.width, self.view.frame.size.height);

    self.centerView.frame = CGRectMake(
                                   (centerWidth / (CGFloat)2) - (centerWidth / (CGFloat)2)
                                   , (self.view.frame.size.height / (CGFloat)2) - (centerWidth / (CGFloat)2)
                                   , centerWidth
                                   , centerWidth
                                   );
    self.centerView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.centerView.tag = 10;
    [self.view addSubview:self.centerView];
}

- (void) addPawns {
    
    static int tagIndex = 201;
    
    CGFloat width = self.centerView.frame.size.width;
    CGFloat step = width / (CGFloat)8;
    CGFloat side = width / (CGFloat)8;
    CGFloat size = 20;
    
    for (int i = 0; i < 8; i++) {
            CGFloat dx = 0 + (side - size)/(CGFloat)2;
            CGFloat dy = i * side + (side - size)/(CGFloat)2;
            for (CGFloat x = dx; x <= width - side/(CGFloat)2 - size/(CGFloat)2; x+=step) {
                CGRect rect = CGRectMake(x, dy, size, size);
                UIView* view = [[UIView alloc] initWithFrame:rect];
                view.tag = tagIndex;
                if ([view tag] <= 224) {
                    view.backgroundColor = [UIColor redColor];
                }
                else if ([view tag] > 240) {
                    view.backgroundColor = [UIColor blueColor];
                }
                else if (([view tag] > 224) && ([view tag] <= 240)){
                    view.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
                }
                view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
                [self.centerView addSubview:view];
                ++tagIndex;
            }
    }
}

- (void) swapPawns {
    
    NSUInteger randomTime = arc4random_uniform(15) + 1;

    for (int i = 0 ; i < randomTime; i++) {
        
        NSInteger randomTagFirstView = 201 + arc4random()%(232 - 201);
        
        NSInteger randomTagSecondView = 232 + arc4random()%(264 - 232);
        
        UIColor* color = [[[self.centerView viewWithTag:randomTagFirstView] backgroundColor] copy];
        
        [[self.centerView viewWithTag:randomTagFirstView]
         setBackgroundColor:[[self.centerView viewWithTag:randomTagSecondView] backgroundColor]];
        
        [[self.centerView viewWithTag:randomTagSecondView]
         setBackgroundColor:color];
    }
}
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {

    [self swapPawns];
}
@end









